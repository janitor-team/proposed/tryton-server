#!/usr/bin/env python3
import locale
import pycountry

locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
selection = [' - '.join(
        [c.name.replace(',', ' '), c.alpha_2]) for c in pycountry.countries]
selection.sort()
print(' '.join(['Choices:',
        str(selection).
            replace("'", '').
            replace('"', '').
            replace('[', '').
            replace(']', '')
            ]))
