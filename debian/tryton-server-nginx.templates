Template: tryton-server-nginx/website-uri
Type: string
_Description: Domain for the Tryton website:
 This will be the domain under which the Tryton server will be exposed via
 http (and/or https with Letsencrypt certficates when using the
 python3-certbot-nginx package).
 .
 The value should be a FQDN (Full Qualified Domain Name). Typically it is
 something like "mycompany.com" or "tryton.mysite.eu".
 .
 If you leave the value empty the hostname of this machine will be used
 as domain.

Template: tryton-server-nginx/run-certbot
Type: boolean
Default: false
_Description: Perform Letsencrpyt registration and configuration?
 When the python3-certbot-nginx package is installed (default configuration)
 the automatic registration with Letsencrypt can be performed.
 The domain as configured in the previous question will be registered.
 .
 Note: For this to work you need 
  - a fully registered domain name
  - working DNS records for this domain name
  - accessibility to this server via Internet on port 80/443
 .
 In case you want to register several (sub-)domains with this nginx frontend
 you should opt out and perform the registration manually.
 You can perform this configuration manually at any time later (please refer
 to the necessary steps in /usr/share/doc/certbot/README.rst.gz).
 .
 When registering with Letsencrypt you agree with the Terms of Service at
 https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf. You must
 agree in order to register with the ACME server.

Template: tryton-server-nginx/certbot-email
Type: string
_Description: Letsencrypt E-Mail:
 Enter an email address (used for urgent renewal and security notices)
