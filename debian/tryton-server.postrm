#!/bin/sh

set -e

TRYTON_USER="tryton"
TRYTON_OLDCONFFILE="/etc/trytond.conf"
TRYTON_CONFDIR="/etc/tryton"
TRYTON_CONFFILE="${TRYTON_CONFDIR}/trytond.conf"
TRYTON_LOGCONFFILE="${TRYTON_CONFDIR}/trytond_log.conf"
TRYTON_CONFFILEPRE34="${TRYTON_CONFDIR}/trytond.conf.pre34"
TRYTON_LOGDIR="/var/log/tryton"
TRYTON_HOMEDIR="/var/lib/tryton"

# POSIX-compliant shell function to check for the existence of a command
# s. developers-reference 6.4
pathfind() {
    OLDIFS="$IFS"
    IFS=:
    for p in $PATH; do
        if [ -x "$p/$*" ]; then
            IFS="$OLDIFS"
            return 0
        fi
    done
    IFS="$OLDIFS"
    return 1
}

case "${1}" in
    purge)
        # Remove evtl. configuration files and backups
        for _ITEM in "${TRYTON_CONFFILEPRE34}" "${TRYTON_OLDCONFFILE}" "${TRYTON_CONFFILE}" "${TRYTON_LOGCONFFILE}"; do
            rm -f "${_ITEM}" > /dev/null 2>&1 || true
            for ext in '~' '%' .bak .ucf-new .ucf-old .ucf-dist; do
                rm -f "${_ITEM}"$ext > /dev/null 2>&1 || true
            done
        done

        # Purge ucf registries
        if pathfind ucf; then
            for _ITEM in "${TRYTON_OLDCONFFILE}" "${TRYTON_CONFFILE}" "${TRYTON_LOGCONFFILE}"; do
                ucf --purge "${_ITEM}"
                ucfr --purge tryton-server "${_ITEM}"
            done
        fi

        # Remove evtl. dpkg-statoverrides
        for _ITEM in "${TRYTON_OLDCONFFILE}" "${TRYTON_CONFFILE}" "${TRYTON_LOGCONFFILE}" "${TRYTON_HOMEDIR}" "${TRYTON_LOGDIR}"; do
            dpkg-statoverride --force --remove "${_ITEM}" > /dev/null 2>&1 || true
        done

        # Remove the system user
        if pathfind deluser; then
            deluser --quiet --system "${TRYTON_USER}"
        fi

        # Removing (potentially) empty directories
        for _ITEM in "${TRYTON_CONFDIR}" "${TRYTON_LOGDIR}" "${TRYTON_HOMEDIR}"; do
            rmdir --ignore-fail-on-non-empty "${_ITEM}" > /dev/null 2>&1 || true
        done
        ;;

    remove|upgrade|failed-upgrade|abort-install|abort-upgrade|disappear)

        ;;

    *)
        echo "postrm called with unknown argument \`${1}'" >&2
        exit 1
        ;;
esac

#DEBHELPER#

exit 0
