#!/bin/sh

db_name="tryton"

# get database settings from dbconfig-common
if [ -f /etc/dbconfig-common/tryton-server-postgresql.conf ]; then
    . /etc/dbconfig-common/tryton-server-postgresql.conf
fi

if [ ! -z $dbc_dbname ]; then
    db_name=$dbc_dbname
fi

/usr/bin/trytond-admin -c /etc/tryton/trytond.conf -d "$db_name"  -u country currency || true
