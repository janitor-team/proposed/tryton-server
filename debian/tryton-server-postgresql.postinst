#!/bin/sh

set -e

. /usr/share/debconf/confmodule
. /usr/share/dbconfig-common/dpkg/postinst

dbc_go tryton-server-postgresql "$@"

TRYTON_USER="tryton"
TRYTON_CONFDIR="/etc/tryton"
TRYTON_SHAREDIR="/usr/share/tryton-server"
TRYTON_CONFFILE="${TRYTON_CONFDIR}/trytond.conf"
TRYTON_CONFTEMPLATE="${TRYTON_SHAREDIR}/default/trytond.conf"
TRYTON_CONFNEW=

cleanup () {
    [ "$TRYTON_CONFNEW" ] && rm -f "$TRYTON_CONFNEW"
}

create_config () {
    trap cleanup EXIT
    TRYTON_CONFNEW=$(mktemp)
    #cp -a "$TRYTON_CONFTEMPLATE" $TRYTON_CONFNEW
    # start from the existing configuration, it could have manually changed settings
    cp -a "$TRYTON_CONFFILE" $TRYTON_CONFNEW
    
    # get database settings from dbconfig-common
    if [ -f /etc/dbconfig-common/tryton-server-postgresql.conf ]; then
        . /etc/dbconfig-common/tryton-server-postgresql.conf
    fi

    case "$dbc_dbtype" in
        pgsql)
            uri=$dbc_dbuser:$dbc_dbpass@$dbc_dbserver
            if [ ! -z $dbc_dbport ]; then
                uri="$uri":$dbc_dbport
            fi
            uri="$uri"/
            # first uncomment the existing postgresql uri sample line
            sed -i -e "s|^#\s*\(uri = postgresql://tryton:tryton@localhost:5432/\)|\1|" "$TRYTON_CONFNEW"
            # now update the active postgresql uri with the correct uri
            sed -i -e "s|^\(uri = postgresql://\)\(tryton:tryton@localhost:5432/\)|\1$uri|" "$TRYTON_CONFNEW"
            ;;
        sqlite3)
            # just use the default sqlite database
            ;;
        "")
            ;;
        *)
            echo "Unsupported database type $dbc_type."
            exit 1
            ;;
    esac
    
    # Re-apply permissions to be safe
    if ! dpkg-statoverride --list "${TRYTON_CONFFILE}" > /dev/null 2>&1
    then
        chown "${TRYTON_USER}":"${TRYTON_USER}" "${TRYTON_CONFNEW}"
        chmod 0440 "${TRYTON_CONFNEW}"
    fi
    
    # Register new config file
    ucf --debconf-ok --src-dir "$TRYTON_SHAREDIR/default/" "$TRYTON_CONFNEW" "$TRYTON_CONFFILE"
    cleanup
}

case "$1" in
    configure)
        create_config
        db_stop
    ;;

    abort-upgrade|abort-remove|abort-deconfigure)
    ;;

    *)
        echo "postinst called with unknown argument \`$1'" >&2
        exit 1
    ;;
esac

#DEBHELPER#

exit 0

